+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------------+-----------------------+
| []                                                                                                                                                                                                                                                                                                                                                |                       |                       |
|                                                                                                                                                                                                                                                                                                                                                   |                       |                       |
| +-----------------------------------+:-------------------------------------:+                                                                                                                                                                                                                                                                     |                       |                       |
| | []                                | LA VILLA DE COLIMA DE LA NUEVA ESPAÑA |                                                                                                                                                                                                                                                                     |                       |                       |
| |                                   | SIGLO XVI                             |                                                                                                                                                                                                                                                                     |                       |                       |
| |                                   |                                       |                                                                                                                                                                                                                                                                     |                       |                       |
| |                                   | VOLUMEN II      (CAJAS 12-20)         |                                                                                                                                                                                                                                                                     |                       |                       |
| +-----------------------------------+---------------------------------------+                                                                                                                                                                                                                                                                     |                       |                       |
+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------------+-----------------------+
| +------------------------------------------------------------------------+-------------------------------+-----------------------+                                                                                                                                                                                                                |                       |                       |
| | Inicio | Búsqueda | Siglas y bibliografía | Créditos | Más información | Vol. I (1-11) Vol. II (12-20) |    [ES]               |                                                                                                                                                                                                                |                       |                       |
| +------------------------------------------------------------------------+-------------------------------+-----------------------+                                                                                                                                                                                                                |                       |                       |
+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------------+-----------------------+
| Búsqueda rápida                                                                                                                                                                                                                                                                                                                                   |                       |                       |
|                                                                                                                                                                                                                                                                                                                                                   |                       |                       |
| +:--------------------:+-----------------+:----------------------------------------:+----------------:+                                                                                                                                                                                                                                           |                       |                       |
| | Documento de fecha:  |                 | Ubicación física del documento original: | [] [] []        |                                                                                                                                                                                                                                           |                       |                       |
| |                      |                 |                                          |                 |                                                                                                                                                                                                                                           |                       |                       |
| | 1581. Septiembre, 13 |                 | Caja 23, exp. 5, 6 ff.                   |                 |                                                                                                                                                                                                                                           |                       |                       |
| +----------------------+-----------------+------------------------------------------+-----------------+                                                                                                                                                                                                                                           |                       |                       |
|                                                                                                                                                                                                                                                                                                                                                   |                       |                       |
|   ------------- ---------------------------------------------------------------------                                                                                                                                                                                                                                                             |                       |                       |
|   Resumen:      Martín Ruiz contra Ana de la Zarza, por una cédula de plazo pasado.                                                                                                                                                                                                                                                               |                       |                       |
|   ------------- ---------------------------------------------------------------------                                                                                                                                                                                                                                                             |                       |                       |
|                                                                                                                                                                                                                                                                                                                                                   |                       |                       |
|   ----------------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |                       |                       |
|   Contenido:                          Ante el alcalde ordinario de la Villa de Colima Alonso Carrillo de Guzmán y Francisco López Avecilla, escribano público de la Villa, Ana de la Zarza, viuda de Diego Morán, reconoce una cédula fechada el 12 de febrero de ese año.                                                                        |                       |                       |
|                                       Por ella reconoce que debe a Martín Ruiz 44 pesos de oro común por un censo, dinero y cosas que éste le había vendido.                                                                                                                                                                                      |                       |                       |
|                                       Ana de la Zarza se obliga a pagarla para la Pascua Florida^([1]) próxima venidera.                                                                                                                                                                                                                          |                       |                       |
|                                       A su ruego, firmó la obligación Lorenzo de Madrid,^([2]) siendo testigos de ello Lorenzo Álvarez de Madrid,^([3]) Gonzalo de Cáceres y Diego Arias.^([4])                                                                                                                                                   |                       |                       |
|                                       Dice que ya pagó 6 de ellos.                                                                                                                                                                                                                                                                                |                       |                       |
|                                       Un año después, el 12 de septiembre de 1582, ante Juan Convergel Maldonado^([5]) y Baltasar de Alcalá, escribano de su juzgado, Martín Ruiz pidió hacer ejecución en los bienes de Ana de la Zarza.                                                                                                         |                       |                       |
|                                       Dado el mandamiento al alguacil mayor de la Villa para proceder a ello, Martín Ruiz señaló por bienes en donde hacer la dicha ejecución, las casas de la morada de la susodicha en esta Villa, “que alindan con solares de Juan Fernández de Ocampo e, por la otra banda, solares del dicho Martín Ruiz”.   |                       |                       |
|                                       Testigos fueron de ello, Juan Serrano Adán y Gaspar Román.                                                                                                                                                                                                                                                  |                       |                       |
|                                       Corrieron a continuación los pregones por voz de Francisco,^([6]) negro pregonero.                                                                                                                                                                                                                          |                       |                       |
|                                       El 3 de diciembre siguiente, el teniente de alcalde mayor Cristóbal de Silva dictó sentencia y falló que debía mandar y mandó avivar la voz de del almoneda a los dichos bienes, estando presentes por testigos Juan Núñez^([7]) y Hernando de Solórzano, vecinos de Colima.                                |                       |                       |
|                                       Gaspar Román se presentó por fiador de Martín Ruiz.                                                                                                                                                                                                                                                         |                       |                       |
|                                       El 10 de diciembre, en presencia del teniente de alcalde mayor, escribano y numerosos testigos, y por voz de Miguel,^([8]) esclavo, pregonero nombrado para este efecto, se hizo el último remate de los bienes ejecutados.                                                                                 |                       |                       |
|                                       El vecino Gaspar Román puso por precio a las casas por 50 pesos de oro común, adjudicándoselas.                                                                                                                                                                                                             |                       |                       |
|                                       El teniente de alcalde mayor mandó hacer tasación de las costas en la forma siguiente: derechos de juez, 6 tomines; del escribano, 3 pesos; del alguacil, 2 pesos; del pregonero, 4 tomines.                                                                                                                |                       |                       |
|                                                                                                                                                                                                                                                                                                                                                   |                       |                       |
|   ----------------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |                       |                       |
|                                                                                                                                                                                                                                                                                                                                                   |                       |                       |
| (1) Semana de Pascua de Resurrección, la siguiente a la Semana Santa.                                                                                                                                                                                                                                                                             |                       |                       |
|                                                                                                                                                                                                                                                                                                                                                   |                       |                       |
| (2) Lorenzo de Madrid, hijo de Andrés de Madrid y Catalina Maldonado, ya era vecino de Colima en 1574: Romero de Solís, Andariegos… (2001), 289-290.                                                                                                                                                                                              |                       |                       |
|                                                                                                                                                                                                                                                                                                                                                   |                       |                       |
| (3) ¿Lorenzo Álvarez de Madrid y Lorenzo de Madrid son el mismo vecino?.                                                                                                                                                                                                                                                                          |                       |                       |
|                                                                                                                                                                                                                                                                                                                                                   |                       |                       |
| (4) Diego Arias de Arellano                                                                                                                                                                                                                                                                                                                       |                       |                       |
|                                                                                                                                                                                                                                                                                                                                                   |                       |                       |
| (5) Juez pesquisidor y de residencia del alcalde mayor Juan Núñez del Castillo y del alguacil Gonzalo Hernández: Romero de Solís, Andariegos… (2001), 120-121.                                                                                                                                                                                    |                       |                       |
|                                                                                                                                                                                                                                                                                                                                                   |                       |                       |
| (6) Si bien en su momento no fue documentado este pregonero, pensamos que se trata del mismo negro Francisco que registramos, en: Romero de Solís, Padrón de negros… (2007), 12, n° 115.                                                                                                                                                          |                       |                       |
|                                                                                                                                                                                                                                                                                                                                                   |                       |                       |
| (7) Juan Núñez como se le acostumbra llamar, es el vecino Juan Núñez de Alvarado El Mozo, hijo de Juan Núñez de Alvarado e Isabel de Arévalo: Romero de Solís, Andariegos… (2001), 364-365.                                                                                                                                                       |                       |                       |
|                                                                                                                                                                                                                                                                                                                                                   |                       |                       |
| (8) Tal vez sea el mismo Miguel, negro pregonero en 1577, que registramos en: Romero de Solís, Padrón de negros… (2007), 13, n° 135.                                                                                                                                                                                                              |                       |                       |
|                                                                                                                                                                                                                                                                                                                                                   |                       |                       |
|   ---------------------------------------------------------------------------------------------------                                                                                                                                                                                                                                             |                       |                       |
|    Documento(s) original(es)                                                                                                                                                                                                                                                                                                                      |                       |                       |
|   --------------------------- ------- ------- ------- ------- ------- ------- ------- ------- -------                                                                                                                                                                                                                                             |                       |                       |
|               []                []      []      []      []      []      []      []      []      []                                                                                                                                                                                                                                                |                       |                       |
|               1f                1v      2f      2v      3f      3v      4f      4v      5f      5v                                                                                                                                                                                                                                                |                       |                       |
|                                                                                                                                                                                                                                                                                                                                                   |                       |                       |
|               []                                                                                                                                                                                                                                                                                                                                  |                       |                       |
|               6f                                                                                                                                                                                                                                                                                                                                  |                       |                       |
|   ---------------------------------------------------------------------------------------------------                                                                                                                                                                                                                                             |                       |                       |
+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------------+-----------------------+
| +----------------------------------------------------------------------------+-----------------------------------------------:+                                                                                                                                                                                                                   |                       |                       |
| | ISBN 978-607-701-024-1                                                     |   ---- ---- ---- ---- ---- ---- ---- ---- ---- |                                                                                                                                                                                                                   |                       |                       |
| | © Derechos Reservados 2013. Archivo Histórico del Municipio de Colima. [*] |   []        []        []        []        []   |                                                                                                                                                                                                                   |                       |                       |
| |                                                                            |   ---- ---- ---- ---- ---- ---- ---- ---- ---- |                                                                                                                                                                                                                   |                       |                       |
| +----------------------------------------------------------------------------+------------------------------------------------+                                                                                                                                                                                                                   |                       |                       |
| |                                                                            |                                                |                                                                                                                                                                                                                   |                       |                       |
| +----------------------------------------------------------------------------+------------------------------------------------+                                                                                                                                                                                                                   |                       |                       |
+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------------+-----------------------+

636 visitas
