[]

LA VILLA DE COLIMA DE LA NUEVA ESPAÑA
SIGLO XVI

VOLUMEN I      (CAJAS 1-11)

[]

Inicio | Búsquedas | Siglas y bibliografía | Cartografía | Créditos | Más información

[]

Búsqueda rápida

+:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:+------------------------------------------:+
| Fecha:                                                                                                                                                                                                                                                                                                                                      | Ubicación física del documento original:  |
| 1555 Febrero, 20                                                                                                                                                                                                                                                                                                                            | CAJA A-2, EXP. 11, 5 FF.                  |
|                                                                                                                                                                                                                                                                                                                                             | Archivo Histórico del Municipio de Colima |
+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------------------------------+
| 43. 1555. Febrero, 20.                                                                                                                                                                                                                                                                                                                      |                                           |
|                                                                                                                                                                                                                                                                                                                                             |                                           |
| Juan de Aguilar reclama de Francisco Preciado el pago de cincuenta pesos que le quedó debiendo su difunta madre.                                                                                                                                                                                                                            |                                           |
|                                                                                                                                                                                                                                                                                                                                             |                                           |
| Visto el testamento de Elvira de Arévalo, mujer que fue de Francisco Preciado, el alcalde ordinario Bartolomé Garrido ordenó a éste que pagase. Juan de Aguilar, además, pidió que se le pagaran 2 pesos y 2 tomines que dio al escribano Diego Veedor por los derechos del testamento. El alcalde mandó a Preciado que también los pagara. |                                           |
|                                                                                                                                                                                                                                                                                                                                             |                                           |
| Francisco Preciado, el mismo día, decía que Juan de Aguilar bajo juramento declarase de qué se le debían los dichos 50 pesos y qué cosas había dado por ellos y a quién las dio, que de otro modo él no pagaría nada, y que si el alcalde le apremiaba, "apelo de su mandamiento para ante el señor alcalde mayor desta Villa".^([1])       |                                           |
|                                                                                                                                                                                                                                                                                                                                             |                                           |
| Garrido respondió que no embargante la apelación, Preciado debía dar y pagar lo que se le mandó.                                                                                                                                                                                                                                            |                                           |
|                                                                                                                                                                                                                                                                                                                                             |                                           |
| Preciado apeló, entonces, al alcalde mayor Alonso Sánchez de Toledo, quien dispuso que se le entregara un traslado de la cláusula del testamento, lo que hizo el escribano Juan de la Torre.                                                                                                                                                |                                           |
|                                                                                                                                                                                                                                                                                                                                             |                                           |
| El testamento otorgado por Elvira de Arévalo en Colima, a 18 de enero de 1555, había pasado ante García Garcés,^([2]) escribano nombrado, y la clásula rezaba así: "Iten mando que se den e paguen cincuenta pesos del oro que corre a Juan de Aguilar, vecino desta dicha Villa, los cuales me prestó para comprar cierta cosa".           |                                           |
|                                                                                                                                                                                                                                                                                                                                             |                                           |
| Sánchez de Toledo confirmó el auto del alcalde ordinario Bartolomé Garrido.                                                                                                                                                                                                                                                                 |                                           |
|                                                                                                                                                                                                                                                                                                                                             |                                           |
| Caja A–2, exp. 11, 5 ff.                                                                                                                                                                                                                                                                                                                    |                                           |
|                                                                                                                                                                                                                                                                                                                                             |                                           |
|                                                                                                                                                                                                                                                                                                                                             |                                           |
|                                                                                                                                                                                                                                                                                                                                             |                                           |
| ---------------                                                                                                                                                                                                                                                                                                                             |                                           |
|                                                                                                                                                                                                                                                                                                                                             |                                           |
| (1) Como se dice líneas adelante, lo era entonces Alonso Sánchez de Toledo.                                                                                                                                                                                                                                                                 |                                           |
|                                                                                                                                                                                                                                                                                                                                             |                                           |
| (2) Garci Garcés de Mansilla, el mismo que un año y meses después, en 1557, asesinaría a Francisco Preciado.                                                                                                                                                                                                                                |                                           |
+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------------------------------+

Documento(s) original(es)

Archivo no encontrado (CAJAS/CAJA2/EXP11/004V.jpg)

[]
1F

[]
1V

[]
2F

[]
2V

[]
3F

[]
3V

[]
4F

[]
5F

[]
5V

Proyecto interinstitucional del Archivo Histórico del Municipio de Colima y la Universidad de Colima,
realizado bajo los auspicios del fondo Ramón Álvarez-Buylla de Aldana.

------------------------------------------------------------------------

ISBN 978-968-7412-85-6
® Derechos Reservados 2008. Archivo Histórico del Municipio de Colima.

1,078 visitas
