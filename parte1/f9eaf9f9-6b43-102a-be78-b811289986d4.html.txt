[]

LA VILLA DE COLIMA DE LA NUEVA ESPAÑA
SIGLO XVI

VOLUMEN I      (CAJAS 1-11)

[]

Inicio | Búsquedas | Siglas y bibliografía | Cartografía | Créditos | Más información

[]

Búsqueda rápida

+:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:+------------------------------------------:+
| Fecha:                                                                                                                                                                                                                                                                          | Ubicación física del documento original:  |
| 1567 Noviembre, 9                                                                                                                                                                                                                                                               | CAJA A-6, EXP. 9, F. 16.                  |
|                                                                                                                                                                                                                                                                                 | Archivo Histórico del Municipio de Colima |
+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------------------------------+
| 206. 1567. Noviembre, 9.                                                                                                                                                                                                                                                        |                                           |
|                                                                                                                                                                                                                                                                                 |                                           |
| Carta de pago otorgada por el escribano Blas López por la saca de un traslado de la curaduría de Juan Preciado.                                                                                                                                                                 |                                           |
|                                                                                                                                                                                                                                                                                 |                                           |
| Con fecha de 9 de noviembre de 1567, el escribano Blas López firma haber recibido de Juan Núñez de Alvarado la cantidad de un peso "por la saca del traslado de la curaduría discernida en Juan Preciado por la Justicia de Colima de los menores hijos de Francisco Preciado". |                                           |
|                                                                                                                                                                                                                                                                                 |                                           |
| Caja A–6, exp. 9, f. 16.                                                                                                                                                                                                                                                        |                                           |
+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------------------------------+

Documento(s) original(es)

Archivo no encontrado (CAJAS/CAJA6/EXP9/016F.jpg)

Archivo no encontrado (CAJAS/CAJA6/EXP9/016V.jpg)

Proyecto interinstitucional del Archivo Histórico del Municipio de Colima y la Universidad de Colima,
realizado bajo los auspicios del fondo Ramón Álvarez-Buylla de Aldana.

------------------------------------------------------------------------

ISBN 978-968-7412-85-6
® Derechos Reservados 2008. Archivo Histórico del Municipio de Colima.

1,078 visitas
