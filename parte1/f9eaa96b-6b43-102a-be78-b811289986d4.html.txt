[]

LA VILLA DE COLIMA DE LA NUEVA ESPAÑA
SIGLO XVI

VOLUMEN I      (CAJAS 1-11)

[]

Inicio | Búsquedas | Siglas y bibliografía | Cartografía | Créditos | Más información

[]

Búsqueda rápida

+:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:+------------------------------------------:+
| Fecha:                                                                                                                                                                                                                                                                                                                                                  | Ubicación física del documento original:  |
| 1569 Septiembre, 15                                                                                                                                                                                                                                                                                                                                     | CAJA A-6, EXP. 9, F. 1.                   |
|                                                                                                                                                                                                                                                                                                                                                         | Archivo Histórico del Municipio de Colima |
+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------------------------------+
| 194. 1569. Septiembre, 15.                                                                                                                                                                                                                                                                                                                              |                                           |
|                                                                                                                                                                                                                                                                                                                                                         |                                           |
| Cristóbal Preciado, menor, pide dineros para vestirse y estudiar.                                                                                                                                                                                                                                                                                       |                                           |
|                                                                                                                                                                                                                                                                                                                                                         |                                           |
| "Cristóbal Preciado, hijo legítimo de Francisco Preciado que sea en gloria, parezco ante V.m. y digo que yo tengo necesidad de cien pesos para me vestir e ir al estudio y para lo necesario de él. Pido y suplico a V.m. mande a Juan Núñez, mi tutor, que de los bienes que me sean pertenescientes, me los dé, dando V.m. su mandamiento para ello". |                                           |
|                                                                                                                                                                                                                                                                                                                                                         |                                           |
| Llamado a declarar el tutor dijo "que al presente no tiene dineros que poder dar al dicho Preciado", pero que los pudiera dar de su propia hacienda, con tal que se le tomen en cuenta y pagárselos luego de sus bienes. Así lo dispuso el alcalde ordinario Melchor Pérez, siendo testigos Juan Preciado y Pero Ruiz de Vilches.                       |                                           |
|                                                                                                                                                                                                                                                                                                                                                         |                                           |
| Cristóbal Preciado firma carta de pago ante Andrés Martel, Nicolás Bote y Juan Martín, vecinos de Colima, con fecha de 21 de septiembre de ese año.                                                                                                                                                                                                     |                                           |
|                                                                                                                                                                                                                                                                                                                                                         |                                           |
| Caja A–6, exp. 9, f. 1.                                                                                                                                                                                                                                                                                                                                 |                                           |
+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------------------------------+

Documento(s) original(es)

Archivo no encontrado (CAJAS/CAJA6/EXP9/001F.jpg)

Archivo no encontrado (CAJAS/CAJA6/EXP9/001V.jpg)

Proyecto interinstitucional del Archivo Histórico del Municipio de Colima y la Universidad de Colima,
realizado bajo los auspicios del fondo Ramón Álvarez-Buylla de Aldana.

------------------------------------------------------------------------

ISBN 978-968-7412-85-6
® Derechos Reservados 2008. Archivo Histórico del Municipio de Colima.

1,078 visitas
