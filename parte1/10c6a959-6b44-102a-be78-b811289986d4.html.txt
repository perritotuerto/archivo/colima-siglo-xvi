[]

LA VILLA DE COLIMA DE LA NUEVA ESPAÑA
SIGLO XVI

VOLUMEN I      (CAJAS 1-11)

[]

Inicio | Búsquedas | Siglas y bibliografía | Cartografía | Créditos | Más información

[]

Búsqueda rápida

+:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:+------------------------------------------:+
| Fecha:                                                                                                                                                                                                                                                                                                                                                                | Ubicación física del documento original:  |
| 1577 Mayo, 4                                                                                                                                                                                                                                                                                                                                                          | CAJA A-9, EXP. 6, F. 6.                   |
|                                                                                                                                                                                                                                                                                                                                                                       | Archivo Histórico del Municipio de Colima |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------------------------------+
| 296. 1577. Mayo, 4.                                                                                                                                                                                                                                                                                                                                                   |                                           |
|                                                                                                                                                                                                                                                                                                                                                                       |                                           |
| El regidor Antonio Carrillo de Guzmán denuncia la existencia de medidas y pesas falsas en la Provincia de Colima.                                                                                                                                                                                                                                                     |                                           |
|                                                                                                                                                                                                                                                                                                                                                                       |                                           |
| El "regidor desta Villa" Antonio Carrillo de Guzmán dijo ante el teniente de alcalde mayor:(1) "A mi noticia es venido que, en muchas partes desta Provincia, hay medias hanegas y almudes falsos, menos(2) de lo que mandan las Pragmáticas.(3) A V.m. pido y suplico, mande que se traigan ante V.m. para que sean vistas y registradas conforme a las ordenanzas". |                                           |
|                                                                                                                                                                                                                                                                                                                                                                       |                                           |
| El teniente de alcalde mandó que "un alguacil traiga las dichas medidas e pesas que se hallaren ante Su Merced. Y así lo proveyó e mandó e lo firmó". Escribano: Francisco López.                                                                                                                                                                                     |                                           |
|                                                                                                                                                                                                                                                                                                                                                                       |                                           |
| Caja A–9, exp. 6, f. 6.                                                                                                                                                                                                                                                                                                                                               |                                           |
|                                                                                                                                                                                                                                                                                                                                                                       |                                           |
|                                                                                                                                                                                                                                                                                                                                                                       |                                           |
|                                                                                                                                                                                                                                                                                                                                                                       |                                           |
| ---------------                                                                                                                                                                                                                                                                                                                                                       |                                           |
|                                                                                                                                                                                                                                                                                                                                                                       |                                           |
| (1) No se menciona el nombre del teniente. Durante 1577, fungieron como tenientes de alcalde mayor Álvaro de Grijalba y Tomás de Herades.                                                                                                                                                                                                                             |                                           |
|                                                                                                                                                                                                                                                                                                                                                                       |                                           |
| (2) Se entiende que tales medidas y pesos eran "menores" a lo fijado por la ley.                                                                                                                                                                                                                                                                                      |                                           |
|                                                                                                                                                                                                                                                                                                                                                                       |                                           |
| (3) A la letra: "plemáticas"; así también se registra en Boyd–Bowman (1971), 709.                                                                                                                                                                                                                                                                                     |                                           |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------------------------------+

Documento(s) original(es)

Archivo no encontrado (CAJAS/CAJA9/EXP6/006V.jpg)

[]
6F

Proyecto interinstitucional del Archivo Histórico del Municipio de Colima y la Universidad de Colima,
realizado bajo los auspicios del fondo Ramón Álvarez-Buylla de Aldana.

------------------------------------------------------------------------

ISBN 978-968-7412-85-6
® Derechos Reservados 2008. Archivo Histórico del Municipio de Colima.

1,078 visitas
