[]

LA VILLA DE COLIMA DE LA NUEVA ESPAÑA
SIGLO XVI

VOLUMEN I      (CAJAS 1-11)

[]

Inicio | Búsquedas | Siglas y bibliografía | Cartografía | Créditos | Más información

[]

Búsqueda rápida

+:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:+------------------------------------------:+
| Fecha:                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | Ubicación física del documento original:  |
| 1555 Mayo, 17                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | CAJA A-2, EXP. 12, FF. 4-6.               |
|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | Archivo Histórico del Municipio de Colima |
+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------------------------------+
| 45. 1555. Mayo, 17.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |                                           |
|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |                                           |
| Juan de Arana pide que Diego de Aguilar le entregue una cadena de oro y una cruz que tiene suyas.                                                                                                                                                                                                                                                                                                                                                                                                                                               |                                           |
|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |                                           |
| Juan de Arana, ante el alcalde ordinario Bartolomé Garrido, reitera una petición que había hecho para que Diego de Aguilar declarase con juramento "si la cadenilla de oro y cruz que tiene Pedro de Vivanco mía, que le emprestó Isabel de Monjaraz, si era mía, el cual declaró ante V.m. que sí. Pido y suplico a V.m. recibir juramento a Martín Ruiz de Monjaraz para que asimismo declare si es mía", y mande por tanto a Diego de Aguilar, "luego me dé y entregue la dicha cadena de oro y cruz que está en poder de Pedro de Vivanco". |                                           |
|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |                                           |
| Martín Ruiz de Monjaraz declaró que la cadenilla y la cruz son de Isabel de Monjaraz y de su marido Juan de Arana.                                                                                                                                                                                                                                                                                                                                                                                                                              |                                           |
|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |                                           |
| Diego de Aguilar explicó que la cadenilla se la dio Isabel de Monjaraz, suegra de este confesante, y le dijo que se la regalaba pero que la cruz era de Juan de Arana, su esposo, y si éste la pedía, que se la dieran, pero que Juan de Arana quería cruz y cadena.                                                                                                                                                                                                                                                                            |                                           |
|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |                                           |
| El alcalde Garrido dio mandamiento a Diego de Aguilar para que hiciera entrega de lo que se le pedía.                                                                                                                                                                                                                                                                                                                                                                                                                                           |                                           |
|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |                                           |
| Éste apeló ante el alcalde mayor.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |                                           |
|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |                                           |
| Caja A–2, exp. 12, ff. 4-6.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |                                           |
+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------------------------------+

Documento(s) original(es)

Archivo no encontrado (CAJAS/CAJA2/EXP12/005V.jpg)

[]
4F

[]
4V

[]
5F

[]
6F

[]
6V

Proyecto interinstitucional del Archivo Histórico del Municipio de Colima y la Universidad de Colima,
realizado bajo los auspicios del fondo Ramón Álvarez-Buylla de Aldana.

------------------------------------------------------------------------

ISBN 978-968-7412-85-6
® Derechos Reservados 2008. Archivo Histórico del Municipio de Colima.

1,078 visitas
